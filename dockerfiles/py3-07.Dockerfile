FROM python:3.7.15-alpine

RUN apk --no-cache add musl-dev linux-headers g++ jpeg-dev zlib-dev libjpeg make;

RUN pip install numpy matplotlib pytest pytest-cov;
