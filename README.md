<a href="https://indico.ictp.it/event/9781/"><img src="https://raw.githubusercontent.com/zelenelez/images/master/International_Centre_for_Theoretical_Physics.png" width="125" height="125" align="right" /></a>

# A2 group: Simulating pedestrian crowds
``SMR 3696`` | Group project 

💬 **Group:** *ALMEROL Ongue Jenny Lynn, OYEWALE Christianah Titilope, MARTÍNEZ ORTÍZ Lázaro, ZELENKOVSKI, Kiril* <br>
👤 **Mentor:** *CORBETTA Alessandro* <br>
📆 **Deadline**: 09-Dec-22 <br> 
📓 **Documentation**: [Pedestrian crowds](https://zelenkastiot.gitlab.io/crowdphysics_2022/)
