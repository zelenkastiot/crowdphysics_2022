# Unit tests and reports

## 🐍 Python 3.7

```{admonition} Dependecies 
`numpy` <br>
`matplotlib` <br>
`pytest` <br>
`pytest-cov` <br>

```
<center>
<iframe src="https://zelenkastiot.gitlab.io/crowdphysics_2022/coverage/py-3.7-cov/index.html" height="350" width="650" style="border:none;margin-bottom: 10px;" frameborder="0" scrolling="yes"></iframe>
</center>

## 🐍 Python 3.8

```{admonition} Dependecies 
`numpy` <br>
`matplotlib` <br>
`pytest` <br>
`pytest-cov` <br>

```
<center>
<iframe src="https://zelenkastiot.gitlab.io/crowdphysics_2022/coverage/py-3.8-cov/index.html" height="350" width="650" style="border:none;margin-bottom: 10px;"  frameborder="0" scrolling="yes"></iframe>
</center>

## 🐍 Python 3.9

```{admonition} Dependecies 
`numpy` <br>
`matplotlib` <br>
`pytest` <br>
`pytest-cov` <br>

```
<center>
<iframe src="https://zelenkastiot.gitlab.io/crowdphysics_2022/coverage/py-3.9-cov/index.html" height="350" width="650" style="border:none;margin-bottom: 10px;" frameborder="0" scrolling="yes"></iframe>
</center>

## 🐍 Python 3.10.0

```{admonition} Dependecies 
`numpy` <br>
`matplotlib` <br>
`pytest` <br>
`pytest-cov` <br>

```
<center>
<iframe src="https://zelenkastiot.gitlab.io/crowdphysics_2022/coverage/py-3.10-cov/index.html" height="350" width="650" style="border:none;margin-bottom: 10px;"  frameborder="0" scrolling="yes"></iframe>
</center>

