# Theoretical background


Simulation pedestrian crowds - 2022

According to Ming-Liang et. al (2014), Crowds which are systems that are complex contains the collection of individuals. For example human groups, animal herds, insect swarms, and vehicle flows which are in the same physical environment. 
There exist collective behaviors in them which is different from those that they would act on when they are alone.

It is challenging in Physics to model and simulate the behvior of pedestrian crowds. It shares deep connection with statistical fluid mechanics, and boasts primary societal relevance.

KEY QUESTIONS
1. Can we simulate the normal and evacuation behavior of crowds in a public facility? 
2. Which emergent behaviors characterize the system?
3. Are there universal physical features in the motions of the individuals?

A common modeling approach in Mathematical-Physics considers pedestrians as active interacting particles (see Heibing and Molnar, Social face model for pedestrian dynamics, Phys. Rev. E, 1995)
During pedestrian movement in crowd, they interact with one another and also with their environment, e.g to avoid collisions.

Let $x_i(t) = (X_i(t), Y_i(t))$ be the position of pedestrian $i$ at time $t>0$ in a crowd of $N$ individuals (i.e. $i=1,…,N$). 
We model the motion with Newton-like Ordinary Differential Equations (ODE) as

$$
   \begin{eqnarray}
            \ddot{x}_i = F(x_i, \dot{x}_i) + \sum_{j\neq i, j=1}^{N} K(x_i, x_j) + E(x_i)
   \end{eqnarray}
$$


Positions are intended in the 2D plane, and the following are considered:

- F regulates propulsion as $F = \frac{v_d(x_i) - \dot{x}_i}{\tau}$ where $v_d(x)$ is a "desired velocity field" and $t$ is a relaxation time.
- K is a pairwise interaction kernel: $K(x_i, x_j) = A \exp{\left( - \frac{\|x_i-x_j\|^2}{R^2} \right)} \frac{x_i-x_j}{\|x_i-x_j\|} \theta (x_i - x_j, v_d),$
i.e. it is a decaying repulsion force from $x_j$ to $x_i$, that is none-zero only when $x_j$ is in the view cone of $x_i$ ("social force"): R is a typical interaction radius (the view cone is given by $\theta (x_i - x_j, v_d)$ which is zero if the angle between $v_d$ and $x_i - x_j$ is larger than e.g. 80 degrees and 1 otherwise).
- E takes into account the repoulsion of objects and the "impermeability" of walls. $E = B\exp{\left(-\frac{d}{R'}\right)}$ , where $d$ is the distance between a pedestrian and a wall, $n$ is the normal vector pointing towards a wall and R' is a distance scale.

