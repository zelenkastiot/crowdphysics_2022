# Animations 


$N=10$, with no noise.

<p align="center">
<img src="https://gitlab.com/zelenkastiot/crowdphysics_2022/-/raw/main/docs/_static/a1_no_factor.gif" width=70%;></img> <br> <br>
<b>Figugre 1:</b> Simple animation for small crowds, no noise. </a>
</p>



$N=10$, with noise.

<p align="center">
<img src="https://gitlab.com/zelenkastiot/crowdphysics_2022/-/raw/main/docs/_static/a2_factor.gif" width=70%;></img> <br> <br>
<b>Figugre 2:</b> Simple animation for small crowds, with white noise. </a>
</p>


$N=60$, with no noise.

<p align="center">
<img src="https://gitlab.com/zelenkastiot/crowdphysics_2022/-/raw/main/docs/_static/a1_no_factor_more.gif" width=70%;></img> <br> <br>
<b>Figugre 3:</b> Simple animation for bigger crowds, no noise.</a>
</p>


$N=60$, with noise.

<p align="center">
<img src="https://gitlab.com/zelenkastiot/crowdphysics_2022/-/raw/main/docs/_static/a2_factor_more.gif" width=70%;></img> <br> <br>
<b>Figugre 4:</b> Simple animation for bigger crowds, with white noise. </a>
</p>


