.. A2Group documentation master file, created by
   sphinx-quickstart on Tue Dec  6 14:45:28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

A2 Group's documentation!
====================================


Content
==============
.. toctree::
   :maxdepth: 2
   :caption: particle dynamics
   
   content/dynamics.md
   content/animations.md


Docstrings
============
.. toctree::
   :maxdepth: 2
   :caption: Functions

   modules


Unit testing
============
.. toctree::
   :maxdepth: 0
   :caption: Python versions testing
   
   content/unit-tests.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
