import pytest
import numpy as np
from crowd_dynamics import initialating_crowds, euler_solver, plot_crowd, get_theta, get_K_rhs

def test_get_theta():
    x = np.array([0., 1., 2., 3.])
    y = np.array([0., 1., 2., 3.])
    v_d = np.array([[1., 0.], [1., 0.], [1., 0.], [1., 0.]] )
    theta = get_theta(x, y, x, y, v_d)
    np.testing.assert_array_almost_equal(theta, np.ones(4))
    
def test_initialating_crowds():
    n_particles = 2
    n_steps = 3
    a = np.ones( n_particles)
    t_arr, _, _, _, _ = initialating_crowds(n_steps, a, a, a, a, n_particles)
    assert np.shape(t_arr)[0]== 4
    
def test_initialating_crowds1():
    n_particles = 2
    n_steps = 3
    a = np.ones( n_particles)
    _, Y_arr, _, _, _ = initialating_crowds(n_steps, a, a, a, a, n_particles)
    assert np.shape(Y_arr)== (4, 2)
    
def test_initialating_crowds2():
    n_particles = 2
    n_steps = 3
    a = np.ones( n_particles)
    _, _, X_arr, _, _ = initialating_crowds(n_steps, a, a, a, a, n_particles)
    assert np.shape(X_arr)== (4, 2)
    
def test_initialating_crowds3():
    n_particles = 2
    n_steps = 3
    a = np.ones( n_particles)
    _, _, _, VY_arr, _ = initialating_crowds(n_steps, a, a, a, a, n_particles)
    assert np.shape(VY_arr)== (4, 2)
    
def test_initialating_crowds4():
    n_particles = 2
    n_steps = 3
    a = np.ones( n_particles)
    _, _, _, _, VX_arr = initialating_crowds(n_steps, a, a, a, a, n_particles)
    assert np.shape(VX_arr)== (4, 2)

def test_get_K_rhs():
    n_particles = 2
    A = 1
    R = 1
    a = np.ones( n_particles)
    a, _ = get_K_rhs(A, a, a, a, a, R, a)
    assert np.shape(a)[0]== n_particles

def test_get_K_rhs1():
    n_particles = 2
    A = 1
    R = 1
    a = np.ones( n_particles)
    _, b = get_K_rhs(A, a, a, a, a, R, a)
    assert np.shape(b)[0]== n_particles
    
def test_get_K_rhs2():
    A = 1
    R = 1
    x = np.array([0., 1.])
    vx = np.array([1., 1.])
    vy = np.array([0., 0.])
    v_d = np.array([[1., 0.], [1., 0.]])
    a, b = get_K_rhs(A, x, x, vx, vy, R, v_d)
    print(a - b)
    assert np.array_equal(a, b)

def test_euler_solver():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    t_arr, _, _, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert np.array_equal(t_arr, dt * np.arange(3))
    
def test_euler_solver1():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, X_arr, _, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (X_arr <= l).all()
    
def test_euler_solver2():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, X_arr, _, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (X_arr >= 0).all()

def test_euler_solver3():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, _, Y_arr, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (Y_arr >= 0).all()

def test_euler_solver4():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, _, Y_arr, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (Y_arr <= L_w).all()

def test_euler_solver5():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, _, Y_arr, _, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (Y_arr <= L_w).all()

def test_euler_solver6():
    v_d = np.array([[1., 0.], [1., 0.]])#, [1., 0.], [1., 0.]] )
    R = 1.0
    A = 1.
    dt = 0.01
    num_p = 2
    tau = 1
    factor = 0
    L_w = 7.
    l = 8.
    n_steps = 2
    x = np.array([[1., 8.],[0., 0.],[0., 0.]])
    t = t_arr = np.zeros(n_steps + 1)
    y = np.array([[1., 7.],[1., 7.],[0., 0.]])
    vx = np.array([[1., 1.], [1., 1.], [1., 1.]])
    vy = np.array([[0., 0.], [1., 1.], [1., 1.]])
    _, _, _, VX_arr, _ = euler_solver(n_steps, dt, t, y, x, vy, vx, v_d, R, A, tau, factor, L_w, l)
    assert (VX_arr == 1).all()