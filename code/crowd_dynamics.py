#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Created on Wed Dec  7 16:04:51 2022

@author: lazaro, jenny, christianah, kiril
"""

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
import math as mt





def initialating_crowds(n_steps, X_0, Y_0, VX_0, VY_0, number_of_particles):
    """
    This return the initial conditions of the sistem 

    Parameters
    ----------
    n_steps : int
        Number of steps.
    X_0 : array
        Initial X positions of each particles.
    Y_0 : array
        Initial Y positions of each particles.
    VX_0 : array
        Initial x_velocity of each particles.
    VY_0 : array
        Initial y_velocity of each particles.
    number_of_particles : int
        The number_of_particles.

    Returns
    -------
    t_arr : array
        The temporal line.
    Y_arr : array
        The y position of each particle for all time step.
    X_arr : array
        The x position of each particle for all time step.
    VY_arr : array
        The y velocity of each particle for all time step.
    VX_arr : array
        The x velocity of each particle for all time step.

    """
    
    Y_arr = np.zeros((n_steps + 1, number_of_particles))
    X_arr = np.zeros((n_steps + 1, number_of_particles))
    VY_arr = np.zeros((n_steps + 1, number_of_particles))
    VX_arr = np.zeros((n_steps + 1, number_of_particles))
    t_arr = np.zeros(n_steps + 1)
    t_arr[0] = 0
    Y_arr[0] = Y_0
    X_arr[0] = X_0
    VY_arr[0] = VY_0
    VX_arr[0] = VX_0
    return t_arr, Y_arr, X_arr, VY_arr, VX_arr


def euler_solver(
    n_steps, dt, t_arr, Y_arr, X_arr, VY_arr, VX_arr, vd, R, A, tau, factor, L_w, l
):
    """
    Solve the Newton-like differential equations that model the motion of
    a pedestrian in a crowd

    Parameters
    ----------
    n_steps : int
        The number of time steps.
    dt : float
        The time interval.
    t_arr : array
        The temporal line.
    Y_arr : array
        The y position of each particle for all time step.
    X_arr : array
        The x position of each particle for all time step.
    VY_arr : array
        The y velocity of each particle for all time step.
    VX_arr : array
        The x velocity of each particle for all time step.
    vd : array
        The desired velocity field.
    R : float
        The interaction radius.
    A : float
        Scale factor.
    tau : float
        The relaxation time.
    factor : float
        Scale factor for the noise.
    L_w : float
        The height of the upper wall.
    l : float
        The lenght of the corridor.

    Returns
    -------
    t_arr : array
        The temporal line.
    Y_arr : array
        The y position of each particle for all time step.
    X_arr : array
        The x position of each particle for all time step.
    VY_arr : array
        The y velocity of each particle for all time step.
    VX_arr : array
        The x velocity of each particle for all time step.

    """

    for i in range(1, n_steps + 1):
        noise_y = np.random.normal(0, dt, np.shape(VY_arr[i]))
        noise_x = np.random.normal(0, dt, np.shape(VX_arr[i]))
        X = X_arr[i - 1]
        Y = Y_arr[i - 1]
        V_Y = VY_arr[i - 1]
        V_X = VX_arr[i - 1]
        t = t_arr[i - 1]
        K_x, K_y = get_K_rhs(A, X, Y, V_X, V_Y, R, vd)
        dVYdt = (
            (vd[:, 1] - V_Y) / tau
            + np.exp(-1 * Y / 0.1)
            - np.exp(-1 * (L_w - Y) / 0.1)
            - 3 * K_y
        )
        dVXdt = (vd[:, 0] - V_X) / tau - 3 * K_x
        VY_arr[i] = V_Y + dt * dVYdt + factor * noise_y
        VX_arr[i] = V_X + dt * dVXdt + factor * noise_x
        X_arr[i] = (X + VX_arr[i - 1] * dt) % l
        Y_arr[i] = Y + VY_arr[i - 1] * dt
        t_arr[i] = t + dt
    return t_arr, X_arr, Y_arr, VX_arr, VY_arr


def plot_crowd(X_arr, Y_arr, x_label_string, y_label_string):
    """
    Return a 2-D plot 

    Parameters
    ----------
    X_arr : array
        X components of an array.
    Y_arr : array
        Y components of an array.
    x_label_string : string
        Name of the x axis.
    y_label_string : string
        Name of the y axis.

    Returns
    -------
    2-D Plot.

    """
    plt.figure()
    plt.xlabel(x_label_string, fontsize=12)
    plt.plot(X_arr, Y_arr, linewidth=3, label="Y")

    plt.ylabel(y_label_string, fontsize=12)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.grid(True)
    plt.show()


def get_theta(x0, y0, x1, y1, v_d):
    '''
    Returns an array of 1s and 0s. 1 if the neighboring particle is within the viewing angle, 0 otherwise.

    Parameters
    ----------
    x0 : array
        x-position of the current particle
    y0 : array
        x-position of the current particle
    x1 : array
        x-position of the neighboring particle
    y1 : array
        y-position of the neighboring particle
    vd : array
        The desired velocity field.

    Returns
    -------
    theta : array
        Array of 1s and 0s. 1 if their is an interaction between the current particle and the neighboring particle.

    '''
    p_vector_x = x0 - x1
    p_vector_y = y0 - y1
    vec_x_mag = np.sqrt(np.square(p_vector_x) + np.square(p_vector_y))
    angle1_ = np.divide(
        p_vector_x, vec_x_mag, out=np.zeros_like(p_vector_x), where=vec_x_mag != 0
    )
    angle1 = np.arccos(angle1_)

    v_d_mag = np.sqrt(np.square(v_d[:, 0]) + np.square(v_d[:, 1]))

    angle2_ = np.divide(
        v_d[:, 0], v_d_mag, out=np.zeros_like(v_d[:, 0]), where=v_d_mag != 0
    )
    angle2 = np.arccos(angle2_)
    angle = np.abs(angle2 - angle1)
    index = angle > 100 * (mt.pi / 180)
    theta = np.ones(len(angle))
    theta[index] = 0
    return theta


def get_K_rhs(A, x, y, vx, vy, R, v_d):
    """
    

    Parameters
    ----------
    A : float
        Scale factor.
    x : array
        The x position of each particle for all time step.
    y : array
        The y position of each particle for all time step.
    vx : array
        The x velocity of each particle for all time step.
    vy : array
        The y velocity of each particle for all time step.   
    R : float
        The interaction radius.
    vd : array
        The desired velocity field.
    Returns
    -------
    TYPE array
        The pairwaise interction kernel.

    """
    x = np.reshape(x, (-1, 1))
    y = np.reshape(y, (-1, 1))

    dist_x = x - x.T
    dist_y = y - y.T

    sqrx = np.square(dist_x)
    sqry = np.square(dist_y)
    norm = np.sqrt(sqrx + sqry)

    x_component = np.divide(dist_x, norm, out=np.zeros_like(dist_x), where=norm != 0)
    y_component = np.divide(dist_y, norm, out=np.zeros_like(dist_y), where=norm != 0)

    # theta = get_theta(new_x, new_y, x[i] * np.ones(len(new_x)), y[i] * np.ones(len(new_y)), nw_vd)

    pair_intxn_x = A * np.exp(-(np.square(norm) / R**2)) * x_component  # * theta
    pair_intxn_y = A * np.exp(-(np.square(norm) / R**2)) * y_component  # * theta

    return np.sum(pair_intxn_x, axis=0), np.sum(pair_intxn_y, axis=0)
