"""

Created on Wed Dec 7 16:04:51 2022

@authors: lazaro, jenny, christianah, kiril
"""

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import numpy as np
from crowd_dynamics import initialating_crowds, euler_solver, plot_crowd

if __name__ == "__main__":
    R = 1.5
    A = 1
    L_w = 7
    l = 8
    factor = 0
    number_of_particles = 200
    crowd_sizes = 4
    tau = 1 / 4
    L = 8.0

    vd = np.zeros((number_of_particles, 2))
    vd[: int(number_of_particles / 2), 0] = -1.0
    vd[int(number_of_particles / 2) :, 0] = 1.0

    X_0 = np.zeros(number_of_particles)
    X_0[: int(number_of_particles / 2)] = 8.0
    Y_0 = np.linspace(0, 7, num=number_of_particles)

    VX_0 = np.zeros(number_of_particles)
    VX_0[: int(number_of_particles / 2)] = -5.0
    VX_0[int(number_of_particles / 2) :] = 5.0

    VY_0 = np.zeros(number_of_particles)
    VY_0[: int(number_of_particles / 2)] = 1.0
    VY_0[int(number_of_particles / 2) :] = -1.0

    n = len(np.arange(0, 200, 0.01))

    t_arr, Y_arr, X_arr, VY_arr, VX_arr = initialating_crowds(
        n, X_0, Y_0, VX_0, VY_0, number_of_particles
    )

    t_arr, X_arr, Y_arr, VX_arr, VY_arr = euler_solver(
        n, 0.01, t_arr, Y_arr, X_arr, VY_arr, VX_arr, vd, R, A, tau, factor, L_w, l
    )

    fig, ax = plt.subplots(1, 1, figsize=(11, 6), dpi=120)
    plt.subplot(1, 1, 1)
    ax.set_xlim(0, 8)
    ax.set_ylim(-1, 8)
    plt.axhline(y=-0.5, xmin=-1, xmax=9, color="gray")
    plt.axhline(y=7.5, xmin=-1, xmax=9, color="gray")

    (graph2,) = plt.plot([], [], "ro", label="_left", markersize=crowd_sizes)
    (graph1,) = plt.plot([], [], "bo", label="_right", markersize=crowd_sizes)

    (graph_trajs,) = plt.plot(
        [], [], "o", color="gray", label="_h1", markersize=0.5, alpha=0.4
    )
    ax.fill_between(
        x=np.linspace(-50, 50), y1=7.5, y2=15.0, color="k", hatch="/", alpha=0.1
    )
    ax.fill_between(
        x=np.linspace(-50, 50), y1=-15, y2=-0.5, color="k", hatch="/", alpha=0.1
    )
    # plt.legend(
    #     loc="upper center",
    #     bbox_to_anchor=(0.9, 1.1),
    #     ncol=2,
    #     fancybox=True,
    #     shadow=True,
    # )
    plt.tick_params(
        axis="both",
        which="both",
        left=False,
        right=False,
        bottom=False,
        top=False,
        labelleft=False,
        labelbottom=False,
    )
    # plt.title(f"N={number_of_particles}, noise_factor={factor}", loc="left")
    tails_x = np.tile(X_arr[0, :], (50, 1)).T
    tails_y = np.tile(Y_arr[0, :], (50, 1)).T

    def update(frame, tails_x, tails_y):

        x = X_arr[frame, :]
        y = Y_arr[frame, :]

        tails_x[:, :-1] = tails_x[:, 1:]
        tails_x[:, -1] = x

        tails_y[:, :-1] = tails_y[:, 1:]
        tails_y[:, -1] = y

        graph1.set_data(
            x[: int(number_of_particles / 2)], y[: int(number_of_particles / 2)]
        )
        graph2.set_data(
            x[int(number_of_particles / 2) :], y[int(number_of_particles / 2) :]
        )

        graph_trajs.set_data(tails_x[:], tails_y[:])

        return graph1, graph2, graph_trajs

    ani = FuncAnimation(
        fig, update, interval=2, repeat=False, blit=True, fargs=(tails_x, tails_y)
    )
    plt.show()
